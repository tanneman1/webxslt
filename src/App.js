import React, { Component } from 'react';
import { formatXml } from './js/FormatUtil';
import { xmlToString, stringToXml } from './js/DomUtil';
import { Button, Alert } from 'react-bootstrap';
import './App.css';

import 'codemirror/lib/codemirror.css';
import 'codemirror/mode/xml/xml';
import { Controlled as CodeMirror } from 'react-codemirror2';

function processXslt(xml, xslt) {
  try {
    const xsltProcessor = new XSLTProcessor();
    xsltProcessor.importStylesheet(xslt);
    return xsltProcessor.transformToFragment(xml, document);
  } catch (e) {
    throw new Error('errors during stylesheet compilation ' + e.message)
  }
}

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      input: ``,
      inputOperation: '',
      output: '',
    };
    this.handleXslt = this.handleXslt.bind(this);
    this.handleXpath = this.handleXpath.bind(this);
    this.handleFormat = this.handleFormat.bind(this);
    this.clearWarning = this.clearWarning.bind(this);
    this.clear = this.clear.bind(this);
  }
  clearWarning() {
    this.setState({
      error: null
    });
  }
  clear() {
    this.clearWarning();
    this.setState({ output: '', input: '', inputOperation: '' });
  }
  handleFormat() {
    this.setState({ input: formatXml(this.state.input) });
    this.setState({ inputOperation: formatXml(this.state.inputOperation) });
  }
  handleXslt() {
    try {
      this.setState({ error: null });
      this.setState({ output: '' });
      const xml = stringToXml(this.state.input.trim());
      const xslt = stringToXml(this.state.inputOperation.trim());
      const output = xmlToString(processXslt(xml, xslt));
      this.setState({ output: formatXml(output) });
    } catch (e) {
      this.setState({ error: e });
      console.error('error', e);
    }
  }
  handleXpath() {
    try {
      this.setState({ error: null });
      this.setState({ output: '' });
      const xml = stringToXml(this.state.input.trim());

      var foundNodes = xml.evaluate( this.state.inputOperation, xml, null, XPathResult.ANY_TYPE,null);
      const result = foundNodes.iterateNext();
      if (result) {
        const extra = foundNodes.iterateNext() ? '\n<!-- more matches found -->' : '';
        const output = xmlToString(result) + extra;
        this.setState({ output });
      } else {
        this.setState({ output: '<!-- no match found -->' });
      }
    } catch (e) {
      this.setState({ error: e });
      console.error('error', e);
    }
  }
  render() {
    var options = {
      lineNumbers: true,
      mode: 'xml'
    };
    return (
      <div className="App">
        {this.state.error !== null &&
          <Alert bsStyle="warning" onDismiss={this.clearWarning}>
            <strong>Holy guacamole!</strong> {this.state.error.message}
          </Alert>
        }
        <div className="upperHalf">
          <div className="half">
            <h3 className="center">XML</h3>
            <CodeMirror className="textareaInput" value={this.state.input} onBeforeChange={(editor, data, input) => {
              this.setState({ input });
            }}
              onChange={(editor, data, value) => { }} options={options} />
          </div>
          <div className="half">
            <h3 className="center">XSLT/XPath</h3>
            <CodeMirror className="textareaInput" value={this.state.inputOperation} onBeforeChange={(editor, data, inputOperation) => {
              this.setState({ inputOperation });
            }}
              onChange={(editor, data, value) => { }} options={options} />
          </ div>
        </ div>
        <div className="center actions">
          <Button bsStyle="primary" bsSize="large" onClick={this.handleXslt} title="Perform XSLT transformation">XSLT</Button>
          <Button bsStyle="primary" bsSize="large" onClick={this.handleXpath} title="Perform XPath query">XPath</Button>
          <Button bsStyle="outline-primary" bsSize="large" onClick={this.handleFormat} title="Format input fields">Format</Button>
          <Button bsStyle="outline-primary" bsSize="large" onClick={this.clear} title="Clear input fields">Clear</Button>
        </div>
        <div className="bottomHalf">
          <CodeMirror value={this.state.output} onChange={(editor, data, value) => { }} options={options} />
        </div>
      </div>
    );
  }
}

export default App;
