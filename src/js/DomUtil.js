export function xmlToString(xmlDom) {
  return new XMLSerializer().serializeToString(xmlDom);
}

export function stringToXml(xmlStr) {
  if (xmlStr.length === 0) {
    throw new Error('input is empty');
  }
  const out = new DOMParser().parseFromString(xmlStr, "text/xml");
  if (out.getElementsByTagName("parsererror").length > 0) {
    throw new Error(out.getElementsByTagName("parsererror")[0].firstChild.textContent);
  }
  return out;
}
